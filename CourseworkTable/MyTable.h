#pragma once
#include "Scroll.h"
#include <Windows.h>
#include <vector>
#include <string>

using namespace std;

struct Action {
	int i, j;
	char* prevValue;

	Action();
	Action(int i, int j, char* prevValue);
	void Set(int i, int j, const char* prevValue);
	~Action();
};

class MyTable {
private:
	vector<string> columns;
	vector<vector<string>> rows;
	vector<vector<string>> oldRows;

	char* filename;
	char* saveFilename;

	vector<int> widthColumns;
	int heightCol;

	int totalWidth;
	int totalHeight;
public:
	MyTable();
	MyTable(vector<string> columns, vector<vector<string>> rows);
	MyTable(char* filename);

	void ReadTable();
	void WriteTable(bool dialog);

	void Show(HWND hWnd, HDC hdc, Scroll* scroll);
	
	void AddColumn(string columnName);
	void AddRow();

	int GetMaxWidthOfColumn(int i);
	pair<int, int> GetPosOfCellByCoords(int x, int y);
	RECT GetEditBoxRect(int i, int j, Scroll* scroll);
	string GetTextForEditBox(int i, int j);

	void SetCellText(int i, int j, char* str);
	bool SelectFile(HWND hWnd, bool open);

	char* GetCurrentFilename();

	~MyTable();
};