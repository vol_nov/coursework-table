#include "MyTable.h"
#include <fstream>

bool(*Compare)(vector<string> v1, vector<string> v2, int col);

MyTable::MyTable() {
	filename = nullptr;
	saveFilename = nullptr;
}

MyTable::MyTable(vector<string> columns, vector<vector<string>> rows) {
	filename = nullptr;
	for (auto it = columns.begin(); it != columns.end(); it++) {
		this->columns.push_back(*it);
	}

	for (auto it = rows.begin(); it != rows.end(); it++) {
		this->rows.push_back(*it);
	}
	saveFilename = nullptr;
}
MyTable::MyTable(char* filename) {
	this->filename = new char[strlen(filename) + 1];
	strcpy(this->filename, filename);
	saveFilename = nullptr;
	ReadTable();
}

void MyTable::ReadTable() {
	columns.clear();
	rows.clear();

	ifstream ifs(filename);
	string line;
	getline(ifs, line);

	
	char* temp = new char[strlen(line.c_str()) + 1];
	strcpy(temp, line.c_str());

	char* delim = "\t";

	char* split = strtok(temp, delim);
	while (split != NULL) {
		columns.push_back(string(split));
		split = strtok(NULL, delim);
	}

	delete temp;

	while (getline(ifs, line)) {
		vector<string> row;

		temp = new char[strlen(line.c_str()) + 1];
		strcpy(temp, line.c_str());

		split = strtok(temp, delim);
		while (split != NULL) {
			row.push_back(string(split));
			split = strtok(NULL, delim);
		}

		rows.push_back(row);
	}
	ifs.close();
}

void MyTable::WriteTable(bool dialog) {
	char* save;
	if (dialog)
		save = saveFilename;
	else
		save = filename;

	ofstream ofs;
	ofs.open(save, std::ofstream::out | std::ofstream::trunc);

	for (auto it = columns.begin(); it != columns.end(); it++) {
		if ((*it).size() == 0)
			ofs << " " << '\t';
		else 
			ofs << (*it).c_str() << '\t';
	}
	ofs << '\n';

	for (auto it = rows.begin(); it != rows.end(); it++) {
		auto row = (*it);
		for (auto it1 = row.begin(); it1 != row.end(); it1++) {
			if ((*it1).size() == 0)
				ofs << " " << '\t';
			else
				ofs << (*it1).c_str() << '\t';
		}
		ofs << '\n';
	}

	ofs.close();
}

void MyTable::Show(HWND hWnd, HDC hdc, Scroll* scroll) {
	if (columns.size() < 1)
		return;

	HFONT hFont = CreateFont(20, 8, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Calibri"));

	SelectObject(hdc, hFont);

	int cellHorMargin = 30, cellVerMargin = 15;
	widthColumns.clear();

	SIZE size;

	totalWidth = 1;
	totalHeight = 1;

	for (int i = 0; i < columns.size(); i++) {
		int length = GetMaxWidthOfColumn(i);

		if (length == 0) {
			int min = 1000000000;
			for (int j = 0; j < i; j++) {
				if (columns.at(j).size() < min && 
					columns.at(j).size() != 0)
					min = columns.at(j).size();
			}
			length = min;
		}

		char* len_str = new char[length + 1];

		memset(len_str, 'a', length);
		len_str[length] = '\0';

		GetTextExtentPoint32A(hdc, len_str, length, &size);
		heightCol = size.cy + cellVerMargin + 1;

		widthColumns.push_back(size.cx + cellHorMargin);

		totalWidth += (size.cx + cellHorMargin + 1);

		delete len_str;
	}

	totalHeight = (size.cy + cellVerMargin + 1) * (rows.size() + 1);

	RECT wRect;
	GetWindowRect(hWnd, &wRect);

	
	scroll->maxWidth = totalWidth - (wRect.right - wRect.left) + 35;
	scroll->maxHeight = totalHeight - (wRect.bottom - wRect.top) + heightCol * 2;
	scroll->dx = ceil(scroll->maxWidth / 100.0);
	scroll->dy = ceil(scroll->maxHeight / 100.0);

	int scrollDx = (wRect.right - wRect.left < totalWidth) ? ((double)scroll->maxWidth*(scroll->counterX) / -100.0) : 0;
	int scrollDy = (wRect.bottom - wRect.top < totalHeight) ? ((double)scroll->maxHeight*(scroll->counterY) / -100.0) : 0;

	MoveToEx(hdc, 1, 1, NULL);
	LineTo(hdc, 1, totalHeight + scrollDy);
	LineTo(hdc, totalWidth + scrollDx, totalHeight + scrollDy);
	LineTo(hdc, totalWidth + scrollDx, 1);
	LineTo(hdc, 1, 1);



	RECT rectPos;

	rectPos.left = 2 + scrollDx;
	rectPos.right = widthColumns.at(0) + rectPos.left;
	rectPos.top = 2 + scrollDy;
	rectPos.bottom = rectPos.top + (heightCol);

	DrawText(hdc, columns.at(0).c_str(), columns.at(0).size(), &rectPos, DT_CENTER | DT_VCENTER);

	for (int i = 1; i < columns.size(); i++) {

		rectPos.left += widthColumns.at(i - 1) + 1;
		rectPos.right += widthColumns.at(i) + 1;
		
		DrawText(hdc, columns.at(i).c_str(), columns.at(i).size(), &rectPos, DT_CENTER | DT_VCENTER);

		MoveToEx(hdc, rectPos.left, 1, NULL);
		LineTo(hdc, rectPos.left, totalHeight);
	}
	
	for (int i = 0; i < rows.size(); i++) {
		rectPos.left = 2 + scrollDx;
		rectPos.right = widthColumns.at(0) + rectPos.left;
		rectPos.top = 2 + scrollDy + (heightCol) * (i + 1);
		rectPos.bottom = rectPos.bottom + (heightCol) * (i + 2);

		

		DrawText(hdc, rows.at(i).at(0).c_str(), rows.at(i).at(0).size(), &rectPos, DT_CENTER | DT_VCENTER);

		for (int j = 1; j < rows.at(i).size(); j++) {
			rectPos.left += widthColumns.at(j - 1) + 1;
			rectPos.right += widthColumns.at(j) + 1;

			DrawText(hdc, rows.at(i).at(j).c_str(), rows.at(i).at(j).size(), &rectPos, DT_CENTER | DT_VCENTER);
		}

		MoveToEx(hdc, 2, rectPos.top, NULL);
		LineTo(hdc, totalWidth + scrollDx, rectPos.top);
	}
}

void MyTable::AddColumn(string columnName) {
	columns.push_back(columnName);
	
	int min = 10000000;
	for (auto it = widthColumns.begin(); it != widthColumns.end(); it++) {
		if (min > (*it))
			min = (*it);
	}

	widthColumns.push_back(min);

	for (auto it = rows.begin(); it != rows.end(); it++) {
		(*it).push_back(string(""));
	}
}

void MyTable::AddRow() {
	vector<string> row;
	for (int i = 0; i < columns.size(); i++) {
		row.push_back(" ");
	}
	rows.push_back(row);
}

int MyTable::GetMaxWidthOfColumn(int i) {

	if (i < 0 || i > columns.size() - 1)
		return -1;

	int maxWidth = columns.at(i).size();

	for (auto it = rows.begin(); it != rows.end(); it++) {
		if ((*it).at(i).size() > maxWidth)
			maxWidth = (*it).at(i).size();
	}

	return maxWidth;
}

pair<int, int> MyTable::GetPosOfCellByCoords(int x, int y) {

	int i = -1, j = -1;

	if (x > totalWidth || y > totalHeight || columns.size() < 1) {
		return pair<int, int>(i, j);
	}
	else {
		i = y / (heightCol + 1);
		
		if (x > 0 && x < widthColumns.at(0))
			return pair<int, int>(i, 0);

		int left = widthColumns.at(0);

		for (int k = 1; k < widthColumns.size(); k++) {
			if (x > left && x < left + widthColumns.at(k))
				return pair<int, int>(i, k);
			left += widthColumns.at(k);
		}
	}

	return pair<int, int>(-1, -1);
}

void MyTable::SetCellText(int i, int j, char* str) {
	if (i == 0) {
		columns[j] = string(str);
	}
	else {
		rows[i - 1][j] = string(str);
	}
}

RECT MyTable::GetEditBoxRect(int i, int j, Scroll * scroll) {
	RECT rect, wRect;

	int scrollDx = ((double)scroll->maxWidth*(scroll->counterX) / -100.0);
	int scrollDy = ((double)scroll->maxHeight*(scroll->counterY) / -100.0);


	if (j == 0) {
		rect.left = 2;
		rect.right = widthColumns.at(0) + 2;
	}
	else {
		rect.left = 2;
		for (int k = 1; k <= j; k++) {
			rect.left += widthColumns.at(k - 1) + 1;
			rect.right = rect.left + widthColumns.at(k);
		}
	}

	rect.left += scrollDx;
	rect.right += scrollDx;

	rect.top = 2 + (heightCol) * i + scrollDy;
	rect.bottom = 2 + (heightCol) * (i + 1) + scrollDy;

	return rect;
}

string MyTable::GetTextForEditBox(int i, int j) {

	if (i == 0) {
		return columns.at(j);
	}
	else {
		return rows.at(i - 1).at(j);
	}

}

bool MyTable::SelectFile(HWND hWnd, bool open) {
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.hInstance = GetModuleHandle(NULL);
	
	char temp[255] = "";
	ofn.lpstrFile = temp;
	ofn.nMaxFile = sizeof(temp);

	if (open) {

		if (GetOpenFileName(&ofn) == TRUE) {
			if (filename != nullptr)
				delete[] filename;
			filename = new char[strlen(temp) + 1];;
			strcpy(filename, temp);
			return true;
		}
		else {
			return false;
		}
	}
	else {
		if (GetSaveFileName(&ofn) == TRUE) {
			if (saveFilename != nullptr)
				delete[] saveFilename;
			saveFilename = new char[strlen(temp) + 1];;
			strcpy(saveFilename, temp);
			return true;
		}
		else {
			return false;
		}
	}


}

MyTable::~MyTable() {
	if (filename != nullptr)
		delete filename;

	if (saveFilename != nullptr)
		delete saveFilename;

	saveFilename = nullptr;
	filename = nullptr;
}

Action::Action() {
	this->i = -1;
	this->j = -1;
	this->prevValue = nullptr;
}

Action::Action(int i, int j, char* prevValue) {
	this->i = i;
	this->j = j;
	this->prevValue = new char[strlen(prevValue) + 1];
	strcpy(this->prevValue, prevValue);
}

void Action::Set(int i, int j, const char* prevValue) {
	this->i = i;
	this->j = j;

	if (this->prevValue != nullptr)
		delete[] this->prevValue;

	this->prevValue = new char[strlen(prevValue) + 1];
	strcpy(this->prevValue, prevValue);
}

Action::~Action() {
	if (this->prevValue != nullptr)
		delete this->prevValue;

	this->prevValue = nullptr;
}


char* MyTable::GetCurrentFilename() {
	return filename;
}