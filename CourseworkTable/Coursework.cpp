// OOPLab1.cpp : Defines the entry point for the application.
//
#include <Windows.h>
#include <Windowsx.h>
#include <iostream>
#include "resource.h"

#include "MyTable.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
CHAR szTitle[MAX_LOADSTRING];                  // The title bar text
CHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WNDPROC wpOldEditProc;
MyTable* myTable;
HWND hWndEditBox;
pair<int, int> index;
Action lastAction;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_OOPLAB1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_OOPLAB1));

	MSG msg;

	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_OOPLAB1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_OOPLAB1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindow("COURSEWORK", "������� ������", WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

LRESULT CALLBACK CustomEditProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_KEYDOWN:
		{
			switch(wParam)
			{
			case VK_RETURN: {
					lastAction.Set(index.first, index.second, myTable->GetTextForEditBox(index.first, index.second).c_str());
					int length = GetWindowTextLength(hWnd);
					char* str = new char[length + 2];
					GetWindowText(hWnd, str, length + 1);
					myTable->SetCellText(index.first, index.second, str);
					ShowWindow(hWnd, SW_HIDE);
					InvalidateRect(GetParent(hWnd), NULL, true);
				}
				return 0;
			case VK_ESCAPE:
				ShowWindow(hWnd, SW_HIDE);
				return 0;
			}
		}
		break;
	}
	CallWindowProc(wpOldEditProc, hWnd, msg, wParam, lParam);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Scroll* scroll;
	
	switch (message)
	{
	case WM_CREATE:
		myTable = new MyTable(/*"data.txt"*/);
		hWndEditBox = CreateWindowEx(WS_EX_CLIENTEDGE,
			"EDIT", "", WS_CHILD | ES_AUTOVSCROLL | ES_AUTOHSCROLL | ES_CENTER,
			10, 60, 100, 25, hWnd, (HMENU)NULL, GetModuleHandle(NULL), NULL);
		wpOldEditProc = (WNDPROC)SetWindowLongPtr(hWndEditBox,
			GWLP_WNDPROC,
			(LONG_PTR)CustomEditProc);
		scroll = new Scroll(hWnd, 2000, 2000);
		ShowWindow(hWndEditBox, SW_HIDE);
		break;
	case WM_LBUTTONDOWN:
		{
			int xPos = GET_X_LPARAM(lParam);
			int yPos = GET_Y_LPARAM(lParam);

			index = myTable->GetPosOfCellByCoords(xPos + scroll->counterX * scroll->maxWidth / 100.0, yPos + scroll->counterY * scroll->maxHeight / 100.0);
			if (index.first != -1 || index.second != -1){
				RECT rect = myTable->GetEditBoxRect(index.first, index.second, scroll);
			
				MoveWindow(hWndEditBox, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, true);
				ShowWindow(hWndEditBox, SW_SHOW);
				SetFocus(hWndEditBox);

				SetWindowText(hWndEditBox, myTable->GetTextForEditBox(index.first, index.second).c_str());
			}
		}
		break;
	case WM_CHAR:
		switch (wParam) {
			case 26: //Ctrl + Z
				myTable->SetCellText(lastAction.i, lastAction.j, lastAction.prevValue);
				InvalidateRect(hWnd, NULL, true);
				break;
		}
		break;
	case WM_VSCROLL: {
			OnVScroll(scroll, hWnd, wParam, lParam);

			if (index.first != -1 && index.second != -1) {
				RECT rect = myTable->GetEditBoxRect(index.first, index.second, scroll);
				MoveWindow(hWndEditBox, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, true);
			}
		}
		break;
	case WM_HSCROLL:
		OnHScroll(scroll, hWnd, wParam, lParam);
		if (index.first != -1 && index.second != -1) {
			RECT rect = myTable->GetEditBoxRect(index.first, index.second, scroll);
			MoveWindow(hWndEditBox, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, true);
		}
		break;
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			myTable->Show(hWnd, hdc, scroll);
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);

			switch (wmId)
			{
			case IDM_OPEN: {
					myTable->SelectFile(hWnd, true);
					myTable->ReadTable();

					myTable->GetCurrentFilename();

					char* seperator = " - ";
					char* headerBuffer = new char[strlen(szTitle) + strlen(seperator) + strlen(myTable->GetCurrentFilename()) + 1];

					strcpy(headerBuffer, szTitle);
					strcat(headerBuffer, seperator);
					strcat(headerBuffer, myTable->GetCurrentFilename());

					SetWindowText(hWnd, headerBuffer);

					InvalidateRect(hWnd, NULL, true);
				}
				break;
			case IDM_UNDO:
				myTable->SetCellText(lastAction.i, lastAction.j, lastAction.prevValue);
				InvalidateRect(hWnd, NULL, true);
				break;
			case IDM_SAVE:
				myTable->WriteTable(false);
				break;
			case IDM_SAVE_AS:
				myTable->SelectFile(hWnd, false);
				myTable->WriteTable(true);
				break;
			case IDM_ADD_COLUMN:
				myTable->AddColumn("");
				InvalidateRect(hWnd, NULL, true);
				break;
			case IDM_ADD_ROW:
				myTable->AddRow();
				InvalidateRect(hWnd, NULL, true);
				break;
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
	case WM_DESTROY:
		delete scroll;
		delete myTable;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;

	}
	return (INT_PTR)FALSE;
}